# [CIS-CAT Pro](https://www.cisecurity.org/cybersecurity-tools/cis-cat-pro) in docker

> This repo contains script and describes steps to create **CIS-CAT Pro** docker container image

## How to use

1. Clone this repo to Linux host with Docker Engine running
    * [Official documentation](https://ciscat-pro-dashboard.docs.cisecurity.org/en/stable/source/Dashboard%20Deployment%20Guide%20for%20Linux/#server) claims _Ubuntu 20.04 server_ to be used

```
cd /tmp
git clone https://gitlab.com/treestyle/public/ciscat.git
```

2. In the repo root direcoty create direcoty `downloads/`, place there `.zip` files for **CIS-CAT Pro** distribution and **licence**, and create symbolic links to the mentioned files using corresponding short names: `ccpd.zip` for **CIS-CAT Pro** distribution, and `license.zip` for **licence**
    * The `downloads/` directory is excluded from git commits via `.gitignore` file
    * Let's assume the following files are downloaded from CIS-CAT WorkBench to `/tmp` directory
        * `CIS-CAT-Dashboard-v3.4.0-linux.zip`
        * `CIS-SecureSuite-Product-License.zip`

```
mkdir /tmp/ciscat/downloads
cp /tmp/CIS-CAT-Dashboard-v3.4.0-linux.zip /tmp/ciscat/downloads/
cp /tmp/CIS-SecureSuite-Product-License.zip /tmp/ciscat/downloads/

cd /tmp/ciscat/downloads
ln -s CIS-CAT-Dashboard-v3.4.0-linux.zip ccpd.zip
ln -s CIS-SecureSuite-Product-License.zip license.zip
```

3. In the repo directory `docker/ccpd-pre` create `.env` file with the enviroment variables for DB password like the following (this is example password with `Pa%%w0rdPa%%w0rd` value)
    * The `.env` file is excluded from git commits via `.gitignore` file

```
cat > /tmp/ciscat/docker/ccpd-pre/.env << _EOF
DB_PASSWORD=Pa%%w0rdPa%%w0rd
DB_PASSWORD_ENCODED=r57a2ojPjZuvntraiM+Nmw\=\=
_EOF
```

* Use the following commands to sequentially create the corresponding images
    * `local/downloads:latest` - image containing downloaded **CIS-CAT Pro** distribution and **licence** to be used as base image for the next images
    * `local/ccpd-pre:latest` - image to install, run (as continer), and stop **CIS-CAT Pro** and its DB service (`mariadbd`)
    * `local/ccpd-post:latest` - image created as commit to changes made by running the **CIS-CAT Pro** and its DB service (`mariadbd`) as continer on the previous step
    * `local/ccpd:latest` - image for **CIS-CAT Pro** (based on the previously created `local/ccpd-post:latest` image) containing only necessary files to run **CIS-CAT Pro** as independent container
```
cd /tmp/ciscat/docker/downloads
make # will create 'local/downloads:latest' image

cd /tmp/ciscat/docker/ccpd-pre
make # will create 'local/ccpd-pre:latest' image, it could take few minutes to finish

cd /tmp/ciscat/docker/ccpd-post
make # will create 'local/ccpd-post:latest' image

cd /tmp/ciscat/docker/ccpd
make # will create 'local/ccpd:latest' image
```